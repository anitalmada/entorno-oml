# Pasos para setear un entorno de desarrollo para OML

Se enumeran a continuación los pasos para instalar una VM con las dependencias necesarias que hacen más ágil la instalación del entorno de desarrollo para [OMniLeads &copy;](https://omnileads.net), un software para contact centers desarrollado bajo licencias libres.

Por el momento está sólo disponible para CentOS 7, en un futuro esperamos poder ampliarlo a otras distribuciones GNU/Linux.

## 1. Instalar VirtualBox

Agregar el repo para CentOS:

`# cd /etc/yum.repos.d/`

`# wget http://download.virtualbox.org/virtualbox/rpm/rhel/virtualbox.repo`

`# yum update`

Chequear que está instalada la última versión del kernel:

`# rpm -qa kernel |sort -V |tail -n 1`

`# uname -r`

Si coinciden los outputs, todo bien. Caso contrario, rebootear para que el sistema tome los cambios.

Para resolver dependencias se debe tener instalado el repositorio EPEL, si no está instalado ejecutar lo siguiente:

`# rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm`

Y se instalan estos paquetes:

`# yum install binutils gcc make patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel dkms`

Por último, se instala VirtualBox 2.5:

`# yum install VirtualBox-5.2`

Y se levantan los módulos correspondientes:

`# service vboxdrv setup`

PARA DEBIAN:

https://wiki.debian.org/VirtualBox#Debian_9_.22Stretch.22 pasos del 1 al 3

Luego de tener instalado, correr

`# vboxconfig` 

## 2. Instalar Vagrant

Descargar el paquete correspondiente (en este ejemplo el de arquitectura de 64 bits) e instalar con yum:

`# yum -y https://releases.hashicorp.com/vagrant/2.1.5/vagrant_2.1.5_x86_64.rpm `

## 3. Crear una carpeta para el proyecto y clonar este repositorio

`~$ mkdir mi-proyecto && cd mi-proyecto`

`~$ git clone https://gitlab.com/anitalmada/entorno-oml.git`

## 4. Editar el Vagrantfile para corregir el path del script de provisioning para que matchee con el lugar en que seteamos el directorio del proyecto. Para ello se debe modificar la línea:

`vm.config.provision "shell", path: "path/hacia/directorio/addomnileads.sh"`

En este punto también se puede modificar la IP en caso que lo deseen.

Iniciar la VM con el Vagrantfile que tenemos en la carpeta del repo:

`~$ vagrant up`

ATENCIÓN: Si Vagrant nos pide que ejecutemos un `vagrant init` **se sobreescribe el Vagrantfile** y se reemplaza con uno básico. Se sugiere hacer backup del Vagrantfile provisto por el repo y reemplazar luego de realizar el init.
Puede ocurrir que la primera vez que ejecutemos este comando nos devuelva un error de sincronización de carpetas por el tipo de filesystem soportado por VirtualBox, en tal caso se vuelve a ejecutar `vagrant up` y omitir el paso 5.

## 5. Pedirle a Vagrant que aprovisione la VM con el script que se encuentra en la misma carpeta del repo:

`~$ vagrant provision`

Se van a ejecutar varios comandos, a menos que Vagrant no encuentre el script.
Cuando termine ejecuta un reboot para tomar cambios en caso que hubiera actualizaciones, por lo cual volverá el prompt pero quizás debamos esperar unos momentos para ingresar a la VM utilizando ssh:

`~$ vagrant ssh`

Datos importantes:

* Por defecto las VM en Vagrant traen un usuario con permisos de sudo sin contraseña (usuario _vagrant_)
* Para escalar privilegios solamente alcanza con `sudo su` y se ingresa en un prompt de root.
* La IP que se setea en el Vagrantfile es la que nos hace falta para poder acceder al servidor de desarrollo, ya que no se puede acceder a localhost de la VM desde el sistema operativo host (aunque se puede hacer redirección de puertos, pero en este caso hay un _workaround_ desde la app).
